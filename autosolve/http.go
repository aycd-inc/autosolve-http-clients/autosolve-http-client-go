package autosolve

import "net/http"

type HttpClient interface {
	Do(r *http.Request) (*http.Response, error)
}

type DefaultHttpClient struct {
	HttpClient
}

func (c *DefaultHttpClient) Do(r *http.Request) (*http.Response, error) {
	return http.DefaultClient.Do(r)
}
