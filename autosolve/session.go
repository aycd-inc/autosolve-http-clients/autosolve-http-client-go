package autosolve

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

const (
	authUrl        = "https://autosolve-dashboard-api.aycd.io/api/v1/auth/generate-token"
	apiUrl         = "https://autosolve-api.aycd.io/api/v1"
	tasksUrl       = apiUrl + "/tasks"
	tasksCreateUrl = tasksUrl + "/create"
	tasksCancelUrl = tasksUrl + "/cancel"
)

var (
	sessionMapLock = sync.Mutex{}
	sessionMap     = map[string]*Session{}
	taskExists     = struct{}{}
)

type Session struct {
	apiKey         string
	authLock       sync.Mutex
	token          string
	tokenExpiresAt int64
	httpClient     HttpClient
	pendingTasks   map[string]struct{}
	tasks          map[string]*TaskResponse
	tasksLock      sync.Mutex
	tasksFetchAt   int64
	debug          bool
}

func NewSession(apiKey string) *Session {
	return NewSessionWithHttpClient(apiKey, &DefaultHttpClient{})
}

func NewSessionWithHttpClient(apiKey string, client HttpClient) *Session {
	sessionMapLock.Lock()
	defer sessionMapLock.Unlock()
	session := sessionMap[apiKey]
	if session == nil {
		session = &Session{
			apiKey:         apiKey,
			authLock:       sync.Mutex{},
			tokenExpiresAt: 0,
			httpClient:     client,
			pendingTasks:   make(map[string]struct{}),
			tasks:          make(map[string]*TaskResponse),
			tasksLock:      sync.Mutex{},
			tasksFetchAt:   0,
			debug:          false,
		}
		sessionMap[apiKey] = session
	}
	return session
}

func (s *Session) SetHttpClient(httpClient HttpClient) {
	s.httpClient = httpClient
}

func (s *Session) EnableDebug() {
	s.debug = true
}

func (s *Session) DisableDebug() {
	s.debug = false
}

func (s *Session) Solve(taskRequest *CreateTaskRequest, timeout time.Duration) (*TaskResponse, error) {
	err := s.send(taskRequest, tasksCreateUrl)
	if err != nil {
		return nil, err
	}
	s.pendingTasks[taskRequest.TaskId] = taskExists
	taskResponse, err := s.waitForTask(taskRequest.TaskId, timeout)
	if err != nil {
		cancelErr := s.CancelManyTasks([]string{taskRequest.TaskId})
		if cancelErr != nil {
			s.log(fmt.Sprintf("error while cancelling task %s: %v", taskRequest.TaskId, cancelErr))
		}
	}
	return taskResponse, err
}

func (s *Session) CancelManyTasks(taskIds []string) error {
	cancelTaskRequest := &CancelTaskRequest{
		TaskIds: taskIds,
	}
	for _, taskId := range taskIds {
		delete(s.pendingTasks, taskId)
	}
	err := s.send(cancelTaskRequest, tasksCancelUrl)
	if err != nil {
		return err
	}
	return nil
}

func (s *Session) CancelAllTasks() error {
	cancelTaskRequest := &CancelTaskRequest{}
	s.pendingTasks = make(map[string]struct{})
	err := s.send(cancelTaskRequest, tasksCancelUrl)
	if err != nil {
		return err
	}
	return nil
}

func (s *Session) waitForTask(taskId string, timeout time.Duration) (*TaskResponse, error) {
	createdAt := time.Now()
	time.Sleep(5 * time.Second)
	after := time.After(timeout)
	for {
		select {
		case <-after:
			return cancelled(taskId, createdAt), nil
		default:
			if _, ok := s.pendingTasks[taskId]; !ok {
				return cancelled(taskId, createdAt), nil
			}
			delay, err := s.fetchTasks()
			if err != nil {
				log.Printf("error while fetching tasks: %v", err)
			} else {
				taskResponse := s.tasks[taskId]
				if taskResponse != nil {
					delete(s.tasks, taskId)
					return taskResponse, nil
				}
			}
			time.Sleep(time.Duration(delay) * time.Second)
		}
	}
}

func cancelled(taskId string, createdAt time.Time) *TaskResponse {
	return &TaskResponse{
		TaskId:    taskId,
		CreatedAt: createdAt.Unix(),
		Status:    Cancelled,
	}
}

func (s *Session) fetchTasks() (int, error) {
	s.tasksLock.Lock()
	defer s.tasksLock.Unlock()
	var nextFetchDelay = 5
	if s.tasksFetchAt <= time.Now().Unix() {
		request, err := http.NewRequest(http.MethodGet, tasksUrl, nil)
		if err != nil {
			return nextFetchDelay, err
		}
		response, err := s.do(request)
		if err != nil {
			return nextFetchDelay, err
		}
		if !isStatus2xx(response.StatusCode) {
			return nextFetchDelay, errors.New("response status code is not 2xx")
		}
		var tasks []TaskResponse
		err = json.NewDecoder(response.Body).Decode(&tasks)
		if err != nil {
			return nextFetchDelay, err
		}
		for _, task := range tasks {
			taskCopy := task
			s.tasks[task.TaskId] = &taskCopy
		}
		if len(tasks) >= 100 {
			nextFetchDelay = 1
		}
		s.tasksFetchAt = time.Now().Unix() + int64(nextFetchDelay)
		s.log(fmt.Sprintf("fetched %d tasks with next delay: %d", len(tasks), nextFetchDelay))
	}
	return nextFetchDelay, nil
}

func (s *Session) send(req interface{}, url string) error {
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return err
	}
	body := bytes.NewBuffer(jsonBytes)
	request, err := http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		return err
	}
	response, err := s.do(request)
	if err != nil {
		return err
	}
	if !isStatus2xx(response.StatusCode) {
		return errors.New("response status code is not 2xx")
	}
	return nil
}

func (s *Session) do(request *http.Request) (*http.Response, error) {
	s.authLock.Lock()
	if s.tokenExpiresAt <= time.Now().Unix() {
		s.log("new auth token is required.")
		err := s.refreshAuthToken()
		if err != nil {
			s.tokenExpiresAt = time.Now().Unix() + 60
		}
		s.authLock.Unlock()
		if err != nil {
			return nil, err
		}
	} else {
		s.authLock.Unlock()
	}
	if s.token == "" {
		return nil, errors.New("auth token is not available")
	}
	request.Header.Add("Authorization", "Token "+s.token)
	return s.httpClient.Do(request)
}

type authResponse struct {
	Token     string `json:"token"`
	ExpiresAt int64  `json:"expiresAt"`
}

func (s *Session) refreshAuthToken() error {
	url := authUrl + "?apiKey=" + s.apiKey
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	response, err := s.httpClient.Do(request)
	if err != nil {
		return err
	}
	if !isStatus2xx(response.StatusCode) {
		return errors.New("response status code is not 2xx")
	}
	var authResponse authResponse
	err = json.NewDecoder(response.Body).Decode(&authResponse)
	if err != nil {
		return err
	}
	s.token = authResponse.Token
	s.tokenExpiresAt = authResponse.ExpiresAt
	s.log(fmt.Sprintf("auth token refreshed with new expiresAt: %d", s.tokenExpiresAt))
	return nil
}

func (s *Session) log(msg string) {
	if s.debug {
		log.Println(msg)
	}
}

func isStatus2xx(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}
