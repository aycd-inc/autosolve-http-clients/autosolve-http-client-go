package autosolve

type CreateTaskRequest struct {
	TaskId           string            `json:"taskId"`
	Url              string            `json:"url"`
	SiteKey          string            `json:"siteKey"`
	Version          int               `json:"version"`
	Action           string            `json:"action"`
	MinScore         float32           `json:"minScore"`
	Proxy            string            `json:"proxy"`
	ProxyRequired    bool              `json:"proxyRequired"`
	UserAgent        string            `json:"userAgent"`
	Cookies          string            `json:"cookies"`
	RenderParameters map[string]string `json:"renderParameters"`
	Metadata         map[string]string `json:"metadata"`
}

type CancelTaskRequest struct {
	TaskIds          []string `json:"taskIds"`
	ResponseRequired bool     `json:"responseRequired"`
}

type TaskResponse struct {
	TaskId    string `json:"taskId"`
	CreatedAt int64  `json:"createdAt"`
	Token     string `json:"token"`
	Status    string `json:"status"`
}

const (
	Success   string = "success"
	Cancelled string = "cancelled"

	ReCaptchaV2Checkbox   int = 0
	ReCaptchaV2Invisible  int = 1
	ReCaptchaV3           int = 2
	GeeTest               int = 5
	ReCaptchaV3Enterprise int = 6
	ReCaptchaV2Enterprise int = 7
	FunCaptcha            int = 8
	GeeTestV4             int = 9
	TextImageCaptcha      int = 10
)
