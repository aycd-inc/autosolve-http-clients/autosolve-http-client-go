package main

import (
	"fmt"
	"gitlab.com/aycd-inc/autosolve-http-clients/autosolve-http-client-go/autosolve"
	"log"
	"os"
	"strconv"
	"testing"
	"time"
)

func TestSend(t *testing.T) {
	apiKey := os.Getenv("API_KEY")
	session := autosolve.NewSession(apiKey)
	session.EnableDebug()
	var taskRequest = &autosolve.CreateTaskRequest{
		TaskId:  "1",
		Url:     "https://recaptcha.autosolve.io/version/1",
		SiteKey: "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",
		Version: autosolve.ReCaptchaV2Checkbox,
	}
	//It is recommended to use a higher timeout, like 300s (5m) in production to accommodate for slower solves, so they are not wasted.
	//Unless your solves are time sensitive, in which case, you can use a lower timeout and the task will be cancelled if it is not solved in time.
	taskResponse, err := session.Solve(taskRequest, 120*time.Second)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	log.Printf("TaskResponse: %v", taskResponse)
}

func TestSendBatch(t *testing.T) {
	apiKey := os.Getenv("API_KEY")
	ses := autosolve.NewSession(apiKey)
	fmt.Println("Initiated session")
	for i := 0; i < 10; i++ {
		go func(i int) {
			taskId := "task_" + strconv.Itoa(i)
			fmt.Println("starting... " + taskId)
			res, err := ses.Solve(&autosolve.CreateTaskRequest{
				TaskId:        taskId,
				Url:           "https://recaptcha.autosolve.io/version/1",
				SiteKey:       "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",
				Version:       3,
				ProxyRequired: false,
			}, 120*time.Second)
			if err != nil {
				fmt.Printf("Error solving task: %s, err: %v\n", taskId, err)
			} else {
				fmt.Printf("Task %s solved: %v\n", taskId, res)
			}
		}(i)
	}
	time.Sleep(5 * time.Minute)
}

func TestSendAndCancel(t *testing.T) {
	apiKey := os.Getenv("API_KEY")
	session := autosolve.NewSession(apiKey)
	session.EnableDebug()
	var taskRequest = &autosolve.CreateTaskRequest{
		TaskId:  "ToBeCancelled",
		Url:     "https://recaptcha.autosolve.io/version/1",
		SiteKey: "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",
		Version: autosolve.ReCaptchaV2Checkbox,
	}
	go func() {
		time.Sleep(2 * time.Second)
		taskIds := []string{"ToBeCancelled"}
		err := session.CancelManyTasks(taskIds)
		if err != nil {
			log.Fatalf("Error: %v", err)
		}
	}()
	//It is recommended to use a higher timeout, like 300s (5m) in production to accommodate for slower solves, so they are not wasted.
	//Unless your solves are time sensitive, in which case, you can use a lower timeout and the task will be cancelled if it is not solved in time.
	taskResponse, err := session.Solve(taskRequest, 120*time.Second)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	log.Printf("TaskResponse: %v", taskResponse)
}

func TestSameSessionUsage(t *testing.T) {
	TestSend(t)
	TestSend(t)
}
